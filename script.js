const url = "https://rickandmortyapi.com/api/character";
fetch(url)
  .then((res) => res.json())
  .then((data) => {
    const rawData = data.results;
    createTen(rawData);

    window.onscroll = function () {
      addScroll(rawData);
    };

    // DELETE CARD ON CLICK
    document
      .getElementById("container")
      .addEventListener("click", function () {
        let target = event.target;
        let length = rawData.length;

        for (i = 0; i < length; i++) {          
          if (target.name == i + 1) {
            let id = target.name;
            let div = document.getElementById(`${i+1}`);
            div.remove();
            console.log(rawData.length);            
            rawData.forEach((element, index) => {              
              if (element.id == id) {                
                rawData.splice(index, 1);                
              }
            });
          }
        }; 
      });

      // SORT BY DATE ASC
      document
      .getElementById("date-asc")
      .addEventListener("click", function () {        
        let length = document.getElementsByClassName("char-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".char-wrp");
          div.remove();
        }
        dateSortAsc(rawData);
        createAll(rawData);
      });

      // SORT BY DATE DESC
      document
      .getElementById("date-desc")
      .addEventListener("click", function () {        
        let length = document.getElementsByClassName("char-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".char-wrp");
          div.remove();
        }
        dateSortDesc(rawData);
        createAll(rawData);
      });

      // SORT BY EPISODE #
      document
      .getElementById("episode-sort-id")
      .addEventListener("click", function () {        
        let length = document.getElementsByClassName("char-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".char-wrp");
          div.remove();
        }
        numberOfEpisodesSort(rawData);
        createAll(rawData);
      });

  })
  .catch((error) => {
    console.log(JSON.stringify(error));
  });


function createEl(character) {
  let container = document.getElementById("container");

  let wrapper = document.createElement("div");
  wrapper.className = "char-wrp";
  wrapper.id = character.id.toString();  

  let charName = document.createElement("p");
  charName.innerText = character.name;
  charName.className = "par charname";

  let charSpecies = document.createElement("p");
  charSpecies.innerText = character.species;
  charSpecies.className = "par";

  let charCreatedPar = document.createElement("p");
  charCreatedPar.innerText = `Created`;
  charCreatedPar.className = "par";

  let charCreated = document.createElement("p");
  charCreated.innerText = character.created.slice(11, 19);
  charCreated.className = "par-text";

  let charLocationPar = document.createElement("p");
  charLocationPar.innerText = `Location`;
  charLocationPar.className = "par";

  let charLocation = document.createElement("p");
  charLocation.innerText = `${character.location.name}`;
  charLocation.className = "par-text";

  let charEpisodesPar = document.createElement("p");
  charEpisodesPar.innerText = `Episodes list`;
  charEpisodesPar.className = "par";

  let charEpisodes = document.createElement("p");
  charEpisodes.innerText = `${episodeNumber(character.episode)}`;
  charEpisodes.className = "par-text";

  let charImg = document.createElement("img");
  charImg.src = character.image;
  charImg.className = "image";

  let deleteBtn = document.createElement("button");
  deleteBtn.innerText = "Delete";  
  deleteBtn.className = "delete-btn";
  deleteBtn.setAttribute("name", character.id.toString());

  wrapper.appendChild(deleteBtn);
  wrapper.appendChild(charImg);
  wrapper.appendChild(charName);
  wrapper.appendChild(charSpecies);
  wrapper.appendChild(charCreatedPar);
  wrapper.appendChild(charCreated);
  wrapper.appendChild(charLocationPar);
  wrapper.appendChild(charLocation);
  wrapper.appendChild(charEpisodesPar);
  wrapper.appendChild(charEpisodes);
  
  container.appendChild(wrapper);
}

const openingElementsNumber = 10;

function addScroll(Object) {
  if (document.getElementsByClassName("char-wrp").length < 19) {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {      
      let length = document.getElementsByClassName("char-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".char-wrp");
          div.remove();
        }
        return Object.map((character) => {
          createEl(character);
        });  
    }
  }
}

function dateSortAsc(Object) {
  Object.sort(function (a, b) {
    if (a.created.slice(11, 19) > b.created.slice(11, 19)) {
      return -1;
    }
    if (a.created.slice(11, 19) < b.created.slice(11, 19)) {
      return 1;
    }
    return 0;
  });
}

function dateSortDesc(Object) {
  Object.sort(function (a, b) {
    if (a.created.slice(11, 19) > b.created.slice(11, 19)) {
      return 1;
    }
    if (a.created.slice(11, 19) < b.created.slice(11, 19)) {
      return -1;
    }
    return 0;
  });
}

function episodeNumber(episode) {
  let episodesList = [];
  episode.map((string) => {
    let stringParts = string.split("/");
    episodesList.push(stringParts[stringParts.length - 1]);
  });
  return episodesList.join(", ");
}

function createTen(Object) {
  return Object.slice(0, openingElementsNumber).map((character) => {
    createEl(character);
  });
}

function createAll(Object) {
  return Object.map((character) => {
    createEl(character);
  });
}

function createOne(Object, i) {
  return Object.slice(i, i + 1).map((character) => {
    createEl(character);
  });
}

function numberOfEpisodesSort(Object) {
  Object.sort(function (a, b) {
    if (a.episode.length > b.episode.length) {
      return -1;
    }
    if (a.episode.length < b.episode.length) {
      return 1;
    }
    return 0;
  }); 
}

function deleteElm (Object, id) {
  for (i = 0; i < rawData.length; i++){
    if (Object[i].character.id == id) {
      Object.splice(i, 1);
    }
  }     
};

